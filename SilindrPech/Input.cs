﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SilindrPech
{
    public class Input
    {
        public double tVnutri { get; set; } // Внутренней поверхности (tвн)		
        public string tVnutri2 {get; set; }
        public double tOkr { get; set; } //Окружающей среды(tокр)		
        public string tOkr2 { get; set; }
        public double r0 { get; set; } //Расстояние от центра до начала первого слоя, м
        public string r02 { get; set; }
        public double Kp { get; set; } //Коэффициент, учитывающий положение поверхности
        public double En { get; set; } // степень черноты
        public string En2 { get; set; }
        public double L { get; set; } // Длина газохода
        public string L2 { get; set; }

        // Толщина слоёв
        public double thickness1 { get; set; }
        public string thickness_1 { get; set; }
        public double thickness2 { get; set; }
        public string thickness_2 { get; set; }
        public double thickness3 { get; set; }
        public string thickness_3 { get; set; }
        public double thickness4 { get; set; }
        public string thickness_4 { get; set; }
        public double thickness5 { get; set; }
        public string thickness_5 { get; set; }

        // Выбор материала

        public double Material_1 { get; set; }
        public double Material_2 { get; set; }
        public double Material_3 { get; set; }
        public double Material_4 { get; set; }
        public double Material_5 { get; set; }

        //Кол-во слоёв
        public double layer { get; set; }

    }
}
