﻿using Microsoft.AspNetCore.Server.Kestrel.Core.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SilindrPech
{
    public class MatClass
    {
        public static void rJl1 (Input it, ViewParam vp)
        {


            double Tsr1 = (it.tVnutri + vp.T1) / 2; //множитель для коэфф-та теплопр-ти 1-го слоя
            if (it.Material_1 == 1)
            {
                vp.Jl1 = 0.448 + 0.2 * Math.Pow(10, -3) * Tsr1;
                vp.Mat_1 = "Шамот легковес";
            }
            if (it.Material_1 == 2)
            {
                vp.Jl1 = 0.674 + 0.183 * Math.Pow(10, -3) * Tsr1;
                vp.Mat_1 = "Динас легковес";
            }
            if (it.Material_1 == 3)
            {
                vp.Jl1 = 0.93 + 0.69 * Math.Pow(10, -3) * Tsr1;
                vp.Mat_1 = "Динас";
            }
            if (it.Material_1 == 4)
            {
                vp.Jl1 = 0.84 + 0.58 * Math.Pow(10, -3) * Tsr1;
                vp.Mat_1 = "Шамот";
            }
            if (it.Material_1 == 5)
            {
                vp.Jl1 = 0.15 + 0.3 * Math.Pow(10, -3) * Tsr1;
                vp.Mat_1 = "Диатомит кирпич";
            }
        }

        public static void rJl2(Input it, ViewParam vp)
        {
            double Tsr2 = (vp.T1 + vp.T2) / 2; //множитель для коэфф-та теплопр-ти 2-го слоя
            if (it.Material_2 == 1)
            {
                vp.Jl2 = 0.448 + 0.2 * Math.Pow(10, -3) * Tsr2;
                vp.Mat_2 = "Шамот легковес";
            }
            if (it.Material_2 == 2)
            {
                vp.Jl2 = 0.674 + 0.183 * Math.Pow(10, -3) * Tsr2;
                vp.Mat_2 = "Динас легковес";
            }
            if (it.Material_2 == 3)
            {
                vp.Jl2 = 0.93 + 0.69 * Math.Pow(10, -3) * Tsr2;
                vp.Mat_2 = "Динас";
            }
            if (it.Material_2 == 4)
            {
                vp.Jl2 = 0.84 + 0.58 * Math.Pow(10, -3) * Tsr2;
                vp.Mat_2 = "Шамот";
            }
            if (it.Material_2 == 5)
            {
                vp.Jl2 = 0.15 + 0.3 * Math.Pow(10, -3) * Tsr2;
                vp.Mat_2 = "Диатомит кирпич";
            }
        }

        public static void rJl3(Input it, ViewParam vp)
        {
            double Tsr3 = (vp.T2 + vp.T3) / 2; //множитель для коэфф-та теплопр-ти 3-го слоя
            if (it.Material_3 == 1)
            {
                vp.Jl3 = 0.448 + 0.2 * Math.Pow(10, -3) * Tsr3;
                vp.Mat_3 = "Шамот легковес";
            }
            if (it.Material_3 == 2)
            {
                vp.Jl3 = 0.674 + 0.183 * Math.Pow(10, -3) * Tsr3;
                vp.Mat_3 = "Динас легковес";
            }
            if (it.Material_3 == 3)
            {
                vp.Jl3 = 0.93 + 0.69 * Math.Pow(10, -3) * Tsr3;
                vp.Mat_3 = "Динас";
            }
            if (it.Material_3 == 4)
            {
                vp.Jl3 = 0.84 + 0.58 * Math.Pow(10, -3) * Tsr3;
                vp.Mat_3 = "Шамот";
            }
            if (it.Material_3 == 5)
            {
                vp.Jl3 = 0.15 + 0.3 * Math.Pow(10, -3) * Tsr3;
                vp.Mat_3 = "Диатомит кирпич";
            }


        }
        public static void rJl4(Input it, ViewParam vp)
        {
            double Tsr4 = (vp.T3 + vp.T4) / 2; //множитель для коэфф-та теплопр-ти 4-го слоя
            if (it.Material_4 == 1)
            {
                vp.Jl4 = 0.448 + 0.2 * Math.Pow(10, -3) * Tsr4;
                vp.Mat_4 = "Шамот легковес";
            }
            if (it.Material_4 == 2)
            {
                vp.Jl4 = 0.674 + 0.183 * Math.Pow(10, -3) * Tsr4;
                vp.Mat_4 = "Динас легковес";
            }
            if (it.Material_4 == 3)
            {
                vp.Jl4 = 0.93 + 0.69 * Math.Pow(10, -3) * Tsr4;
                vp.Mat_4 = "Динас";
            }
            if (it.Material_4 == 4)
            {
                vp.Jl4 = 0.84 + 0.58 * Math.Pow(10, -3) * Tsr4;
                vp.Mat_4 = "Шамот";
            }
            if (it.Material_4 == 5)
            {
                vp.Jl4 = 0.15 + 0.3 * Math.Pow(10, -3) * Tsr4;
                vp.Mat_4 = "Диатомит кирпич";
            }
        }
        public static void rJl5(Input it, ViewParam vp)
        {
            double Tsr5 = (vp.T4 + vp.T5) / 2; //множитель для коэфф-та теплопр-ти 5-го слоя
            if (it.Material_5 == 1)
            {
                vp.Jl5 = 0.448 + 0.2 * Math.Pow(10, -3) * Tsr5;
                vp.Mat_5 = "Шамот легковес";
            }
            if (it.Material_5 == 2)
            {
                vp.Jl5 = 0.674 + 0.183 * Math.Pow(10, -3) * Tsr5;
                vp.Mat_5 = "Динас легковес";
            }
            if (it.Material_5 == 3)
            {
                vp.Jl5 = 0.93 + 0.69 * Math.Pow(10, -3) * Tsr5;
                vp.Mat_5 = "Динас";
            }
            if (it.Material_5 == 4)
            {
                vp.Jl5 = 0.84 + 0.58 * Math.Pow(10, -3) * Tsr5;
                vp.Mat_5 = "Шамот";
            }
            if (it.Material_5 == 5)
            {
                vp.Jl5 = 0.15 + 0.3 * Math.Pow(10, -3) * Tsr5;
                vp.Mat_5 = "Диатомит кирпич";
            }
        }



        static public ViewParam calculating(Input it)
                
            {


            it.tVnutri = Double.Parse(it.tVnutri2.Replace('.', ','));
            it.tOkr = Double.Parse(it.tOkr2.Replace('.', ','));
            it.r0 = Double.Parse(it.r02.Replace('.', ','));
            it.En = Double.Parse(it.En2.Replace('.', ','));
            it.L = Double.Parse(it.L2.Replace('.', ','));

            it.thickness1 = Double.Parse(it.thickness_1.Replace('.', ','));
            it.thickness2 = Double.Parse(it.thickness_2.Replace('.', ','));
            it.thickness3 = Double.Parse(it.thickness_3.Replace('.', ','));
            it.thickness4 = Double.Parse(it.thickness_4.Replace('.', ','));
            it.thickness5 = Double.Parse(it.thickness_5.Replace('.', ','));


            ViewParam vp = new ViewParam();

            double Delta = 1; //Погрешность
            vp.T1 = 500; //t1 промежуточный
            vp.T2 = 250; //t2 промежуточный tн
            vp.T3 = 200; //t3
            vp.T4 = 120; //t4
            vp.T5 = 70; //t5

            double R1 =0;
            double R2 = 0;
            double R3 = 0;
            double R4 = 0;
            double R5 = 0;


            // Для двух слоёв
            if (it.layer ==2)
            {
            for (double i = 0; i <= 250; i++)
            {
                if (Math.Abs(Delta) > 0.001)
                {

                    rJl1(it, vp);
                    rJl2(it, vp);

             double A; //коэффициент теплоотдачи
             double A11 = vp.T2 + 273;
             double A22 = it.tOkr + 273;
             A = 2.4 * Math.Pow(vp.T2 - it.tOkr, 0.25) + (5.668 * Math.Pow(10, -8) * it.En * (Math.Pow(A11, 4) - Math.Pow(A22, 4)) / (vp.T2 - it.tOkr));

            R1 = it.r0 + it.thickness1;
            R2 = R1 + it.thickness2; 

        //    double Qk; //Тепловой поток, отдаваемый наружной поверхностью стенки в окружающую среду

            vp.Qk = A * (vp.T2 - it.tOkr) * 2 * Math.PI * it.L * R2;

                    //  double Qt; //Тепловой поток, передаваемый через стенку теплопроводностью
                    double qt111 = R1 / it.r0;
                    double qt222 = R2 / R1;
                    double qt333 = Math.Log(qt111) / (vp.Jl1 * 2 * Math.PI);
                    double qt444 = Math.Log(qt222) / (vp.Jl2 * 2 * Math.PI);

                    vp.Qt = (it.tVnutri - vp.T2) * it.L / (qt333 + qt444);

           // double Q; //Тепловой поток, передаваемый через стенку в окружающую среду

            double q111 = R1 / it.r0;
            double q222 = R2 / R1;
            double q333 = Math.Log(q111) / (vp.Jl1 * 2 * Math.PI);
            double q444 = Math.Log(q222) / (vp.Jl2 * 2 * Math.PI);
            double q555 = 1 / (2 * Math.PI * R2 * A);

            vp.Q = (it.tVnutri - it.tOkr) * it.L / (q333 + q444 + q555);

                    Delta = (vp.Qt - vp.Qk) / vp.Q;
                    double Tgranisa1111 = R1 / it.r0;
                    double Tgranisa2222 = R2 / R1;

                    //Расчет значения температур на границе слоев
                    vp.T1 = it.tVnutri - (vp.Q / it.L) * Math.Log(Tgranisa1111) / (2* Math.PI* vp.Jl1);
                    vp.T2 = vp.T1 - (vp.Q / it.L) * Math.Log(Tgranisa2222) / (2 * Math.PI * vp.Jl2);
   
                }
                else
                {
                    i = 250;
                }
            }
            }

            // Для трёх слоёв
            if (it.layer == 3)
            {
                for (double i = 0; i <= 250; i++)
                {
                    if (Math.Abs(Delta) > 0.001)
                    {

                        rJl1(it, vp);
                        rJl2(it, vp);
                        rJl3(it, vp);
                        

                        double A; //коэффициент теплоотдачи
                        double A11 = vp.T3 + 273;
                        double A22 = it.tOkr + 273;
                        A = 2.4 * Math.Pow(vp.T3 - it.tOkr, 0.25) + (5.668 * Math.Pow(10, -8) * it.En * (Math.Pow(A11, 4) - Math.Pow(A22, 4)) / (vp.T3 - it.tOkr));

                         R1 = it.r0 + it.thickness1;
                         R2 = R1 + it.thickness2;
                         R3 = R2 + it.thickness3;

                        //double Qk; //Тепловой поток, отдаваемый наружной поверхностью стенки в окружающую среду

                        vp.Qk = A * (vp.T3 - it.tOkr) * 2 * Math.PI * it.L * R3;

                        //  double Qt; //Тепловой поток, передаваемый через стенку теплопроводностью
                        double qt111 = R1 / it.r0;
                        double qt222 = R2 / R1;
                        double qt223 = R3 / R2;

                        double qt333 = Math.Log(qt111) / (vp.Jl1 * 2 * Math.PI);
                        double qt444 = Math.Log(qt222) / (vp.Jl2 * 2 * Math.PI);
                        double qt555 = Math.Log(qt223) / (vp.Jl3 * 2 * Math.PI);

                        vp.Qt = (it.tVnutri - vp.T3) * it.L / (qt333 + qt444+ qt555);

                        // double Q; //Тепловой поток, передаваемый через стенку в окружающую среду
                        double q333 = Math.Log(qt111) / (vp.Jl1 * 2 * Math.PI);
                        double q444 = Math.Log(qt222) / (vp.Jl2 * 2 * Math.PI);
                        double q666 = Math.Log(qt223) / (vp.Jl3 * 2 * Math.PI);

                        double q555 = 1 / (2 * Math.PI * R3 * A);

                        vp.Q = (it.tVnutri - it.tOkr) * it.L / (q333 + q444 + q666+ q555);

                        Delta = (vp.Qt - vp.Qk) / vp.Q;


                        //Расчет значения температур на границе слоев
                        vp.T1 = it.tVnutri - (vp.Q / it.L) * Math.Log(qt111) / (2 * Math.PI * vp.Jl1);
                        vp.T2 = vp.T1  - (vp.Q / it.L) * Math.Log(qt222) / (2 * Math.PI * vp.Jl2);
                        vp.T3 = vp.T2  - (vp.Q / it.L) * Math.Log(qt223) / (2 * Math.PI * vp.Jl3);


                    }
                    else
                    {
                        i = 250;
                    }
                }
            }


            // Для 4-х слоёв
            if (it.layer == 4)
            {
                for (double i = 0; i <= 250; i++)
                {
                    if (Math.Abs(Delta) > 0.001)
                    {

                        rJl1(it, vp);
                        rJl2(it, vp);
                        rJl3(it, vp);
                        rJl4(it, vp);

                        double A; //коэффициент теплоотдачи
                        double A11 = vp.T4 + 273;
                        double A22 = it.tOkr + 273;
                        A = 2.4 * Math.Pow(vp.T4 - it.tOkr, 0.25) + (5.668 * Math.Pow(10, -8) * it.En * (Math.Pow(A11, 4) - Math.Pow(A22, 4)) / (vp.T4 - it.tOkr));

                         R1 = it.r0 + it.thickness1;
                         R2 = R1 + it.thickness2;
                         R3 = R2 + it.thickness3;
                         R4 = R3 + it.thickness4;

                        //double Qk; //Тепловой поток, отдаваемый наружной поверхностью стенки в окружающую среду

                        vp.Qk = A * (vp.T4 - it.tOkr) * 2 * Math.PI * it.L * R4;

                        //  double Qt; //Тепловой поток, передаваемый через стенку теплопроводностью
                        double qt111 = R1 / it.r0;
                        double qt222 = R2 / R1;
                        double qt223 = R3 / R2;
                        double qt224 = R4 / R3;

                        double qt333 = Math.Log(qt111) / (vp.Jl1 * 2 * Math.PI);
                        double qt444 = Math.Log(qt222) / (vp.Jl2 * 2 * Math.PI);
                        double qt555 = Math.Log(qt223) / (vp.Jl3 * 2 * Math.PI);
                        double qt666 = Math.Log(qt224) / (vp.Jl4 * 2 * Math.PI);

                        vp.Qt = (it.tVnutri - vp.T4) * it.L / (qt333 + qt444 + qt555+ qt666);

                        // double Q; //Тепловой поток, передаваемый через стенку в окружающую среду


                        double q555 = 1 / (2 * Math.PI * R4 * A);

                        vp.Q = (it.tVnutri - it.tOkr) * it.L / (qt333 + qt444 + qt555 + qt666 + q555);

                        Delta = (vp.Qt - vp.Qk) / vp.Q;


                        //Расчет значения температур на границе слоев
                        vp.T1 = it.tVnutri - (vp.Q / it.L) * Math.Log(qt111) / (2 * Math.PI * vp.Jl1);
                        vp.T2 = vp.T1 - (vp.Q / it.L) * Math.Log(qt222) / (2 * Math.PI * vp.Jl2);
                        vp.T3 = vp.T2 - (vp.Q / it.L) * Math.Log(qt223) / (2 * Math.PI * vp.Jl3);
                        vp.T4 = vp.T3 - (vp.Q / it.L) * Math.Log(qt224) / (2 * Math.PI * vp.Jl4);


                    }
                    else
                    {
                        i = 250;
                    }
                }
            }


            // Для 5и слоёв
            if (it.layer == 5)
            {
                for (double i = 0; i <= 250; i++)
                {
                    if (Math.Abs(Delta) > 0.001)
                    {

                        rJl1(it, vp);
                        rJl2(it, vp);
                        rJl3(it, vp);
                        rJl4(it, vp);
                        rJl5(it, vp);

                        double A; //коэффициент теплоотдачи
                        double A11 = vp.T5 + 273;
                        double A22 = it.tOkr + 273;
                        A = 2.4 * Math.Pow(vp.T5 - it.tOkr, 0.25) + (5.668 * Math.Pow(10, -8) * it.En * (Math.Pow(A11, 4) - Math.Pow(A22, 4)) / (vp.T5 - it.tOkr));

                         R1 = it.r0 + it.thickness1;
                         R2 = R1 + it.thickness2;
                         R3 = R2 + it.thickness3;
                         R4 = R3 + it.thickness4;
                         R5 = R4 + it.thickness5;

                        //double Qk; //Тепловой поток, отдаваемый наружной поверхностью стенки в окружающую среду

                        vp.Qk = A * (vp.T5 - it.tOkr) * 2 * Math.PI * it.L * R5;

                        //  double Qt; //Тепловой поток, передаваемый через стенку теплопроводностью
                        double qt111 = R1 / it.r0;
                        double qt222 = R2 / R1;
                        double qt223 = R3 / R2;
                        double qt224 = R4 / R3;
                        double qt225 = R5 / R4;

                        double qt333 = Math.Log(qt111) / (vp.Jl1 * 2 * Math.PI);
                        double qt444 = Math.Log(qt222) / (vp.Jl2 * 2 * Math.PI);
                        double qt555 = Math.Log(qt223) / (vp.Jl3 * 2 * Math.PI);
                        double qt666 = Math.Log(qt224) / (vp.Jl4 * 2 * Math.PI);
                        double qt777 = Math.Log(qt225) / (vp.Jl5 * 2 * Math.PI);

                        vp.Qt = (it.tVnutri - vp.T5) * it.L / (qt333 + qt444 + qt555 + qt666 + qt777);

                        // double Q; //Тепловой поток, передаваемый через стенку в окружающую среду


                        double q555 = 1 / (2 * Math.PI * R5 * A);

                        vp.Q = (it.tVnutri - it.tOkr) * it.L / (qt333 + qt444 + qt555 + qt666 + qt777 + q555);

                        Delta = (vp.Qt - vp.Qk) / vp.Q;


                        //Расчет значения температур на границе слоев
                        vp.T1 = it.tVnutri - (vp.Q / it.L) * Math.Log(qt111) / (2 * Math.PI * vp.Jl1);
                        vp.T2 = vp.T1 - (vp.Q / it.L) * Math.Log(qt222) / (2 * Math.PI * vp.Jl2);
                        vp.T3 = vp.T2 - (vp.Q / it.L) * Math.Log(qt223) / (2 * Math.PI * vp.Jl3);
                        vp.T4 = vp.T3 - (vp.Q / it.L) * Math.Log(qt224) / (2 * Math.PI * vp.Jl4);
                        vp.T5 = vp.T4 - (vp.Q / it.L) * Math.Log(qt225) / (2 * Math.PI * vp.Jl5);


                    }
                    else
                    {
                        i = 250;
                    }
                }
            }
            vp.T0 = it.tVnutri;

            vp.R0 = it.r0;
            vp.R1 = R1;
            vp.R2 = R2;
            vp.R3 = R3;
            vp.R4 = R4;
            vp.R5 = R5;

            return vp;
        }
    }
}
