﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SilindrPech
{
    public class ViewParam
    {
        //Коэффициент теплопроводности
        public double Jl1 { get; set; }
        public double Jl2 { get; set; }
        public double Jl3 { get; set; }
        public double Jl4 { get; set; }
        public double Jl5 { get; set; }

        //температура границ слоёв
        public double T0 { get; set; }
        public double T1 { get; set; }
        public double T2 { get; set; }
        public double T3 { get; set; }
        public double T4 { get; set; }
        public double T5 { get; set; }

        public double Qk { get; set; } //Тепловой поток, отдаваемый наружной поверхностью стенки в окружающую среду
        public double Qt { get; set; } //Тепловой поток, передаваемый через стенку теплопроводностью
        public double Q { get; set; } //Тепловой поток, передаваемый через стенку в окружающую среду
         
        //Материалы названия
        public string Mat_1 { get; set; }
        public string Mat_2 { get; set; }
        public string Mat_3 { get; set; }
        public string Mat_4 { get; set; }
        public string Mat_5 { get; set; }

        //Радиус слоёв
        public double R0 { get; set; }
        public double R1 { get; set; }
        public double R2 { get; set; }
        public double R3 { get; set; }
        public double R4 { get; set; }
        public double R5 { get; set; }



    }
}
